package main

import (
	"bytes"
	"encoding/json"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
)

var (
	svc     = new()
	request *http.Request
)

// TestHealthHandler - tests the health endpoint
func TestHealthHandler(t *testing.T) {
	ts := httptest.NewUnstartedServer(svc.router)
	svc.initRoutes()
	svc.router.Use(prometheusMiddleware)
	l, _ := net.Listen("tcp", ":8080")
	ts.Listener = l
	ts.Start()
	defer ts.Close()

	resp, err := http.Get("http://0.0.0.0:8080/health")
	if err != nil {
		t.Errorf("error on request: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("got %v, want %v", resp.StatusCode, http.StatusOK)
	}
}

// TestEchoHandler - tests the echo endpoint
func TestEchoHandler(t *testing.T) {
	ts := httptest.NewUnstartedServer(svc.router)
	svc.initRoutes()
	svc.router.Use(prometheusMiddleware)
	l, _ := net.Listen("tcp", ":8080")
	ts.Listener = l
	ts.Start()
	defer ts.Close()

	values := map[string]string{"foo": "bar"}

	jsonValue, _ := json.Marshal(values)

	resp, err := http.Post("http://0.0.0.0:8080/echo", "application/json", bytes.NewBuffer(jsonValue))

	if err != nil {
		t.Errorf("error on request: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("got %v, want %v", resp.StatusCode, http.StatusOK)
	}
}
