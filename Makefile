# IMAGE_NAME - name of the docker image
IMAGE_NAME := registry.gitlab.com/silenterror/service-echo:latest

# ARCH - the architecture
ARCH := $(shell uname -m)

# SERVICE_NAME - name of the service
SERVICE_NAME := service-echo

# PORT - port for the service
PORT ?= 8080

# ----------------------------------------------------------------------------
# Standard Targets
# ----------------------------------------------------------------------------
#
# build - builds the binary 
build:
	go build -a \
		-ldflags '-s -w -linkmode external -extldflags "-static" -X service.BuildTime=$(shell date -u +%Y-%d-%m-%T) -X service.Name=service-echo' \
		-o service-echo \
		main.go 

# deps - installs the binary dependancies
deps:
	@echo "[INFO]: installing dependencies"
	@rm -f go.sum
	@export GO111MODULE=on; \
	go mod download; \
	go mod vendor


# execute - executes the binary
execute:
	SERVICE_NAME=$(SERVICE_NAME) ARCH=$(ARCH) PORT=$(PORT) ./service-echo


# gotest - runs the go tests
gotest:
	go test ./...

# image - builds the image
image:
	docker build . -t $(IMAGE_NAME) 

## lint - will lint the code
lint:
	@if [ ! -f $(shell go env GOPATH)/bin/golangci-lint ]; then \
		wget -O - https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s -- -b $(shell go env GOPATH)/bin v$(GOLANGCI_LINT_RELEASE); \
	fi
	export GO111MODULE=$(GO111MODULE); \
	golangci-lint run -D unused -D deadcode --deadline=10m ./...

# push - pushes the image to docker hub
push:
	docker push $(IMAGE_NAME)

