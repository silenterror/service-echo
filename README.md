# service-echo

## service-echo is a small microservice for use in poc work and testing.

Use Case
--------
This service was built for testing in mind as well as simplicity.  Particularly if a service is needed
during poc work. A service that has metrics and logging were also a requirement for
this service.

Features
--------

Health endpoint - health check
Index endpoint - returns build banner
Echo endpoint - returns a posted payload
Metrics endpoint - returns metrics for the service


Dependancies
------------
Golang 1.12 and >
GNU Make
Binary Dependancies - make deps

Download Dependancies
---------------------
```bash
make deps
```

Build Binary
------------

```bash
make build
```


Build Docker Image
------------------

```bash
make image
```

Run Tests
---------

```bash
make gotest
```

Run the Microservice (once binary is built)
-------------------------------------------
```bash
make execute
```

Run the image (once image is built)
-----------------------------------
```bash
docker run -p 8080:8080 local_service-echo:latest
```

### Service Calls

Health
------

```bash
curl 0.0.0.0:8080/health
```

Echo
----

```bash
curl -X POST 0.0.0.0:8080/echo -d '{"foo":"bar"}'
```


Metrics
------

```bash
curl 0.0.0.0:8080/health
```
