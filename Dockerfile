FROM alpine:latest
ARG PORT
ENV PORT $PORT
RUN apk add --no-cache make curl;
RUN mkdir /opt/service-echo
WORKDIR /opt/service-echo
COPY ["Makefile", "service-echo", "/opt/service-echo/"]
ENTRYPOINT ["./service-echo"]
