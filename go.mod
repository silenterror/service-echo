module gitlab.com/service-echo

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/maxbrunsfeld/counterfeiter/v6 v6.3.0 // indirect
	github.com/prometheus/client_golang v1.8.0
	github.com/rs/zerolog v1.20.0
)
