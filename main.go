package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	requestDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "service_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -o ./test/fakes/fakeservice.go . Service
// Service - interface for the service
type Service interface {
	EchoHandler(w http.ResponseWriter, r *http.Request)
	HealthHandler(w http.ResponseWriter, r *http.Request)
}

type service struct {
	port   string
	router *mux.Router
	logger zerolog.Logger
}

// EchoHandler returns a succesful status and returns what it is receives.
func (s *service) EchoHandler(w http.ResponseWriter, r *http.Request) {
	var b interface{}
	err := json.NewDecoder(r.Body).Decode(&b)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	resp, err := json.Marshal(b)
	if err != nil {
		s.respondErrorHandler(w, http.StatusInternalServerError, err.Error())
	}

	s.logger.Info().Msg(string(resp))

	s.responseHandler(w, http.StatusOK, string(resp))

}

// HealthHandler returns a succesful status and a message.
func (s *service) HealthHandler(w http.ResponseWriter, r *http.Request) {
	s.logger.Info().Msg("health endpoint called")
	s.responseHandler(w, http.StatusOK, `{"healthy":true}`)
}

// initRoutes - initializes the service routes
func (s *service) initRoutes() {
	s.router.Path("/health").HandlerFunc(s.HealthHandler)
	s.router.Path("/echo").HandlerFunc(s.EchoHandler).Methods("POST")
	s.router.Path("/metrics").Handler(promhttp.Handler())
}

// respondErrorHandler - responds with an error
func (s *service) respondErrorHandler(w http.ResponseWriter, code int, message string) {
	s.responseHandler(w, code, map[string]string{"error": message})
}

// responseHandler - sends responses back to the caller
func (s *service) responseHandler(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, err := w.Write(response)
	if err != nil {
		s.logger.Error().Msg("error writing response")
		w.WriteHeader(http.StatusInternalServerError)
	}
}

// prometheusMiddleware implements mux.MiddlewareFunc for obtaining request time
// metrics
func prometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		route := mux.CurrentRoute(r)
		path, _ := route.GetPathTemplate()
		timer := prometheus.NewTimer(requestDuration.WithLabelValues(path))
		next.ServeHTTP(w, r)
		timer.ObserveDuration()
	})
}

func (s *service) run() {
	if s.port == "" {
		s.port = "8080"
	}

	srv := &http.Server{Addr: fmt.Sprintf(":%s", s.port), Handler: s.router}

	go func() {
		s.initRoutes()
		s.router.Use(prometheusMiddleware)
		if err := srv.ListenAndServe(); err != nil {
			log.Err(err)
		}
	}()

	s.logger.Info().Msgf("service listening on port %v", s.port)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c

	wait := time.Duration(5)
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	log.Print("service is shutting down")
	err := srv.Shutdown(ctx)
	if err != nil {
		os.Exit(1)
	}
	os.Exit(0)
}

func new() *service {
	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8080"
	}

	logger := zerolog.New(os.Stdout).With().Timestamp().Logger()

	zerolog.TimeFieldFormat = time.RFC3339
	zerolog.TimestampFieldName = "timestamp"
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	logger = logger.With().Caller().Logger()

	s := &service{
		port:   port,
		router: &mux.Router{},
		logger: logger,
	}
	return s
}

func main() {
	log.Print("service starting")
	s := new()
	s.run()
	os.Exit(0)
}
